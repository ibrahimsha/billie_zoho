# Billie - Developing a Single Page App with FastAPI and Vue.js (Match made in Heaven 🔥)
https://www.billie.tk -> To check some data -> username : test , password : password
https://bitbucket.org/ibrahimsha/billie_zoho/src/master/services/

### Want to learn how to build this?

Check out the [post](https://testdriven.io/blog/developing-a-single-page-app-with-fastapi-and-vuejs).

## Want to use this project? (Local setup)

Build the images and spin up the containers:

```sh
$ docker-compose up -d --build
```

Apply the migrations:

```sh
$ docker-compose exec backend aerich upgrade
```

For the countries field to appear in invoice create and edit there is a CSV file which got to be loaded manually in Database. I used Navicat Import feature. But it's optional

```
billie_zoho/services/backend/src/database/countries.csv
```

## Frontend development
It is easier to get the hot reload from local instead relying from docker container. Make sure to update CORS section in main.py for this to work.
```
cd services/frontend
npm install
npm run serve
```

## API Docs
```
http://194.163.132.59:5000/docs
```
## Database Schema
Schema can be found from
```
billie_zoho/services/backend/migrations/models
```
Also I've added image and pdf versions of schema.
```
billie_zoho/services/backend/billie-schema.PNG
billie_zoho/services/backend/billie-schema.pdf 

```


## Folder arrangement
```
.
├── backend
│   ├── migrations
│   │   └── models
│   └── src
│       ├── auth
│       ├── crud
│       ├── database
│       ├── routes
│       └── schemas
└── frontend
    ├── node_modules
    ├── public
    └── src
        ├── assets
        ├── components
        ├── plugins
        ├── router
        ├── store
        │   └── modules
        └── views
            └── Invoice

```


## TODO:
1. ~~The site is dead slow because it is being serving from (npm run serve) development server. Fixing it by getting the build code from npm run build and serving the /dist folder from nginx for front end~~ 
2. Clean up - You can see that this repo boilerplate code got from _testdriven.io_ it had notes crud which needs to be cleaned up. But it's hidden from UI.
3. Items - It's a Json field in invoices table. I did this to speed things up. This should be a separate table for further scaling.
3. Bulk delete bug - Dashboard check multiple invoices then delete without refreshing the page deleting more will cause issue (check console).
4. Download PDF - When items count reaches more than a A4 document size it won't be visible in the generated pdf.
5. Update FastAPI Invoice Schema to get better documentation from [http://194.163.132.59:5000/docs](http://194.163.132.59:5000/docs).
6. Mobile responsiveness needs to done.

## Current deployment method:
[https://www.billie.tk](https://www.billie.tk), [https://www.billie.tk/api](https://www.billie.tk/api) -> This is deployed in a VPS. It has an apache installed which I'm using as a proxy pass to expose to the web. 
I have attached the conf file which is used on the host.
```
billie_zoho/services/backend/apache2live.conf
```

Ensure [http://localhost:5000](http://localhost:5000), [http://localhost:5000/docs](http://localhost:5000/docs), and [http://localhost:8080](http://localhost:8080) work as expected.

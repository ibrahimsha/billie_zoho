from cgi import test
from typing import List, Dict, Any

from fastapi import APIRouter, Depends, HTTPException
from tortoise.contrib.fastapi import HTTPNotFoundError
from tortoise.exceptions import DoesNotExist

import src.crud.invoices as crud
from src.auth.jwthandler import get_current_user
from src.schemas.invoices import CountriesOutSchema, InvoiceOutSchema
from src.schemas.token import Status
from src.schemas.users import UserOutSchema


router = APIRouter()


@router.get(
    "/countries",
    response_model=List[CountriesOutSchema],
    dependencies=[Depends(get_current_user)],
)
async def get_countries():
    return await crud.get_countries()

@router.get(
    "/invoice/{invoice_id}",
    response_model=InvoiceOutSchema,
    dependencies=[Depends(get_current_user)],
)
async def get_invoice(invoice_id: int) -> InvoiceOutSchema:
    try:
        return await crud.get_invoice(invoice_id)
    except DoesNotExist:
        raise HTTPException(
            status_code=404,
            detail="Note does not exist",
        )

@router.get(
    "/invoices",
    response_model=List[InvoiceOutSchema],
    dependencies=[Depends(get_current_user)],
)
async def get_invoices(current_user: UserOutSchema = Depends(get_current_user)):
    return await crud.get_invoices(current_user)

@router.post(
    "/invoice", response_model=InvoiceOutSchema, dependencies=[Depends(get_current_user)]
)
async def create_invoice(
    invoice: Dict[Any, Any] = None, current_user: UserOutSchema = Depends(get_current_user)
) -> InvoiceOutSchema:
    return await crud.create_invoice(invoice, current_user)

@router.patch(
    "/invoice/{invoice_id}",
    dependencies=[Depends(get_current_user)],
    response_model=InvoiceOutSchema,
    responses={404: {"model": HTTPNotFoundError}},
)
async def update_invoice(
    invoice_id: int,
    invoice: Dict[Any, Any],
    current_user: UserOutSchema = Depends(get_current_user),
) -> InvoiceOutSchema:
    return await crud.update_invoice(invoice_id, invoice, current_user)


@router.post(
    "/invoices",
    response_model=Status,
    responses={404: {"model": HTTPNotFoundError}},
    dependencies=[Depends(get_current_user)],
)
async def delete_invoices(
    invoice_ids: List[int], current_user: UserOutSchema = Depends(get_current_user)
):
    return await crud.delete_invoices(invoice_ids, current_user)

@router.delete(
    "/invoice/{invoice_id}",
    response_model=Status,
    responses={404: {"model": HTTPNotFoundError}},
    dependencies=[Depends(get_current_user)],
)
async def delete_invoice(
    invoice_id: int, current_user: UserOutSchema = Depends(get_current_user)
):
    return await crud.delete_invoices([invoice_id], current_user)


# @router.get(
#     "/note/{note_id}",
#     response_model=NoteOutSchema,
#     dependencies=[Depends(get_current_user)],
# )
# async def get_note(note_id: int) -> NoteOutSchema:
#     try:
#         return await crud.get_note(note_id)
#     except DoesNotExist:
#         raise HTTPException(
#             status_code=404,
#             detail="Note does not exist",
#         )


# @router.post(
#     "/notes", response_model=NoteOutSchema, dependencies=[Depends(get_current_user)]
# )
# async def create_note(
#     note: NoteInSchema, current_user: UserOutSchema = Depends(get_current_user)
# ) -> NoteOutSchema:
#     return await crud.create_note(note, current_user)


# @router.patch(
#     "/note/{note_id}",
#     dependencies=[Depends(get_current_user)],
#     response_model=NoteOutSchema,
#     responses={404: {"model": HTTPNotFoundError}},
# )
# async def update_note(
#     note_id: int,
#     note: UpdateNote,
#     current_user: UserOutSchema = Depends(get_current_user),
# ) -> NoteOutSchema:
#     return await crud.update_note(note_id, note, current_user)


# @router.delete(
#     "/note/{note_id}",
#     response_model=Status,
#     responses={404: {"model": HTTPNotFoundError}},
#     dependencies=[Depends(get_current_user)],
# )
# async def delete_note(
#     note_id: int, current_user: UserOutSchema = Depends(get_current_user)
# ):
#     return await crud.delete_note(note_id, current_user)

from typing import Optional

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from src.database.models import Countries, Invoices


CountriesOutSchema = pydantic_model_creator(
    Countries, name="Country", exclude =[
      "modified_at", "created_at"
    ]
)

InvoiceOutSchema = pydantic_model_creator(
    Invoices, name="Invoice", exclude =[
      "modified_at", "created_at", "created_by",
    ]
)


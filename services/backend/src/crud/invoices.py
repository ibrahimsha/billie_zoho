from fastapi import HTTPException
from tortoise.exceptions import DoesNotExist

from src.database.models import Countries, Invoices
from src.schemas.invoices import CountriesOutSchema, InvoiceOutSchema
from src.schemas.token import Status


async def get_countries():
    return await CountriesOutSchema.from_queryset(Countries.all())

async def get_invoices(current_user):
    return await InvoiceOutSchema.from_queryset(Invoices.filter(created_by_id=current_user.id).order_by('-id'))

async def get_invoice(invoice_id) -> InvoiceOutSchema:
    return await InvoiceOutSchema.from_queryset_single(Invoices.get(id=invoice_id))

async def create_invoice(invoice_dict, current_user) -> InvoiceOutSchema:
    invoice_dict["created_by_id"] = current_user.id
    invoice_obj = await Invoices.create(**invoice_dict)
    return await InvoiceOutSchema.from_tortoise_orm(invoice_obj)

async def update_invoice(invoice_id, invoice, current_user) -> InvoiceOutSchema:
    try:
        db_invoice = await InvoiceOutSchema.from_queryset_single(Invoices.get(id=invoice_id))
    except DoesNotExist:
        raise HTTPException(status_code=404, detail=f"invoice {invoice_id} not found")

    if db_invoice.created_by_id == current_user.id:
        data = without_keys(invoice, *['id','from_country','to_country'])
        invoice = Invoices.filter(id=invoice_id)
        await Invoices.filter(id=invoice_id).update(**data)
        return await InvoiceOutSchema.from_queryset_single(Invoices.get(id=invoice_id))

    raise HTTPException(status_code=403, detail=f"Not authorized to update")

def without_keys(d, *keys):
     return dict(filter(lambda key_value: key_value[0] not in keys, d.items()))

async def delete_invoices(invoices, current_user) -> InvoiceOutSchema:
    for invoice in invoices:
        try:
            invoice_obj = await Invoices.get(id=invoice)
        except DoesNotExist:
            raise HTTPException(status_code=404, detail="Invoice not found")

        if invoice_obj.created_by_id == current_user.id:
                await invoice_obj.delete()

    return Status(message="Invoices Deleted")
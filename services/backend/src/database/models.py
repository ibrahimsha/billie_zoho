from tortoise import fields, models


class Users(models.Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=20, unique=True)
    full_name = fields.CharField(max_length=50, null=True)
    password = fields.CharField(max_length=128, null=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)


class Notes(models.Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=225)
    content = fields.TextField()
    author = fields.ForeignKeyField("models.Users", related_name="note")
    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)

    def __str__(self):
        return f"{self.title}, {self.author_id} on {self.created_at}"

class Countries(models.Model):
    id = fields.IntField(pk=True)
    code = fields.CharField(max_length=225)
    name = fields.CharField(max_length=225)
    nice_name = fields.CharField(max_length=225)
    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)

class Invoices(models.Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=225)
    created_by = fields.ForeignKeyField("models.Users", related_name="invoice")
    
    from_company = fields.CharField(max_length=225,null=True)
    from_name = fields.CharField(max_length=225,null=True)
    from_address_line_1 = fields.CharField(max_length=225,null=True)
    from_address_line_2 = fields.CharField(max_length=225,null=True)
    from_country = fields.ForeignKeyField("models.Countries", related_name="invoice_from", null=True)
    
    to_company = fields.CharField(max_length=225,null=True)
    to_name = fields.CharField(max_length=225,null=True)
    to_address_line_1 = fields.CharField(max_length=225,null=True)
    to_address_line_2 = fields.CharField(max_length=225,null=True)
    to_country = fields.ForeignKeyField("models.Countries", related_name="invoice_to", null=True)


    invoice_date = fields.DatetimeField(auto_now_add=True,null=True)
    due_date = fields.DatetimeField(auto_now_add=True,null=True)

    items = fields.JSONField()

    notes = fields.TextField(null=True)
    terms = fields.TextField(null=True)

    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)

    def __str__(self):
        return f"{self.title}, {self.created_by} on {self.created_at}"

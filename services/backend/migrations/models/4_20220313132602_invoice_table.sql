-- upgrade --
CREATE TABLE IF NOT EXISTS "countries" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "code" VARCHAR(225) NOT NULL,
    "name" VARCHAR(225) NOT NULL,
    "nice_name" VARCHAR(225) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "modified_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);;
CREATE TABLE IF NOT EXISTS "invoices" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "title" VARCHAR(225) NOT NULL,
    "from_company" VARCHAR(225),
    "from_name" VARCHAR(225),
    "from_address_line_1" VARCHAR(225),
    "from_address_line_2" VARCHAR(225),
    "to_company" VARCHAR(225),
    "to_name" VARCHAR(225),
    "to_address_line_1" VARCHAR(225),
    "to_address_line_2" VARCHAR(225),
    "invoice_date" TIMESTAMPTZ   DEFAULT CURRENT_TIMESTAMP,
    "due_date" TIMESTAMPTZ   DEFAULT CURRENT_TIMESTAMP,
    "items" JSONB NOT NULL,
    "notes" TEXT,
    "terms" TEXT,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "modified_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "created_by_id" INT NOT NULL REFERENCES "users" ("id") ON DELETE CASCADE,
    "from_country_id" INT REFERENCES "countries" ("id") ON DELETE CASCADE,
    "to_country_id" INT REFERENCES "countries" ("id") ON DELETE CASCADE
);-- downgrade --
DROP TABLE IF EXISTS "countries";
DROP TABLE IF EXISTS "invoices";

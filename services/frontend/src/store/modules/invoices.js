import axios from 'axios';
import moment from "moment";

const getDefaultState = () => {
  return {
    title: "INVOICE",
    from_company: "",
    from_name: "",
    from_address_line_1: "",
    from_address_line_2: "",
    from_country_id: null,
    to_company: "",
    to_name: "",
    to_address_line_1: "",
    to_address_line_2: "",
    to_country_id: null,
    invoice_date: null,
    due_date: null,
    items: [
      {
        id: 1,
        lable: "Brochure Design",
        qty: 2,
        rate: 100.0,
        amount: 200.0,
      },
    ],
    notes: "It was great doing business with you.",
    terms: "Please make the payment by the due date.",
  }
}

const state = {
  form: getDefaultState(),
  countries: null,
  invoices: null,
  table_invoices: null,
  pageIndex : 1,
  pageSize : 10,

};

const getters = {
  stateCountries: state => state.countries,
  stateForm: state => state.form,
  stateInvoices: state => state.invoices,
  tableInvoices: state => state.table_invoices,
  getPageIndex: state => state.pageIndex,
  getPageSize: state => state.pageSize,
};

const actions = {
  async pullCountries({commit}) {
    let {data} = await axios.get(`countries`);
    commit('setCountires', data);
  },
  async createInvoice({dispatch}, invoice) {
    await axios.post('invoice', invoice.form);
    await dispatch('getInvoices');
  },
  async getInvoice({commit},invoice_id) {
    if(invoice_id){
      let {data} = await axios.get(`invoice/${invoice_id}`);
      commit('setInvoice', data);
    }else{
      commit('resetState');
    }
  },
  async getInvoices({commit}) {
    let {data} = await axios.get(`invoices`);
    commit('setInvoices', data);
    commit('setTableInvoices');
  },
  async deleteInvoices({dispatch,commit},invoice_ids) {
    invoice_ids = JSON.parse(JSON.stringify(invoice_ids))
    await axios.post(`invoices`,invoice_ids);
    await dispatch('getInvoices');
    commit('setTableInvoices');
  },
  async deleteInvoice({dispatch,commit},invoice_id) {
    await axios.delete(`invoice/${invoice_id}`,);
    await dispatch('getInvoices');
    commit('setTableInvoices');
  },
  async pageNumberChange({commit}, pageIndex){
    await commit('setpageIndex', pageIndex);
    commit('setTableInvoices');
  },
  async pageSizeChange({commit}, pageSize){
    await commit('setpageSize', pageSize);
    commit('setTableInvoices');
  },
  // eslint-disable-next-line no-empty-pattern
  async updateInvoice({},invoice){
    await axios.patch(`invoice/${invoice.form.id}`, invoice.form);
  }
};

const mutations = {
  setCountires(state, countries){
    state.countries = countries;
  },
  setInvoice(state, invoice){
      if(invoice.from_country){
        invoice.from_country_id = invoice.from_country.id
      }
      if(invoice.to_country){
        invoice.to_country_id = invoice.to_country.id
      }
      state.form = invoice;
  },
  resetState(state){
    state.form = getDefaultState();
  },
  setInvoices(state, invoices){
    state.invoices = invoices;
    state.invoices.forEach(invoice => {
      if(invoice.invoice_date){
        invoice.invoice_date = moment(String(invoice.invoice_date)).format("DD-MMM-YYYY");
      }
      if(invoice.due_date){
        invoice.due_date = moment(String(invoice.due_date)).format("DD-MMM-YYYY");
      }
    });
  },
  setpageIndex(state,pageIndex){
    state.pageIndex = pageIndex;
  },
  setpageSize(state,pageSize){
    state.pageIndex = 1;
    state.pageSize = pageSize;
  },
  setTableInvoices(state){
    state.table_invoices = state.invoices.slice(
      (state.pageIndex - 1) * state.pageSize,
    state.pageIndex * state.pageSize);
  }

};

export default {
  state,
  getters,
  actions,
  mutations
};
